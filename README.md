# 使用 Docker 快速搭建 danmp 环境

运行环境集成了apache + nginx + mysql + redis + mongo + php(5.6/7.2/7.3) + composer + portainer：
1. `100%`开源
2. `100%`遵循Docker标准
3. 支持**多版本PHP**共存，可任意切换（PHP5.6、PHP7.2)
4. 支持绑定**任意多个域名**
5. 支持**HTTPS和HTTP/2**
6. 默认安装`pdo_mysql`、`redis`、`xdebug`、`swoole`等常用热门扩展，拿来即用
7. 实际项目中应用，确保`100%`可用
8. 一次配置，**Windows、Linux、MacOs**皆可用

### docker-compose.yml 里注释的你有用的上的直接去掉注释 build 运行即可。本地开发数据库可运行在容器里，如果线上运行不建议数据库运行在容器里，必定中间隔了层vm。

### 目录结构

```markdown
conf   配置文件的目录，包括
conf/apache/extra/httpd-vhosts.conf 配置虚拟主机的文件
conf/apache/extra/httpd-ssl.conf 配置https的主机的文件
conf/nginx/conf.d/demo.conf 配置nginx的主机的文件， 也可以新加文件，但是后缀以.conf结尾

data 数据目录 
    --- mysql mysql数据目录 
    --- portainer portainer数据目录

log 日志目录
    --- apache apache日志目录
    --- nginx  nginx日志目录
    --- php-fpm php-fpm日志目录

www    网站的根目录
docker-compose.yml  容器的编译文件
.evn   变量文件

```

### php73 dockerfile 编译文件
[链接地址](https://gitee.com/myxingke/php73Dockerfile "链接地址")

### php72 dockerfile 编译文件
[链接地址](https://gitee.com/myxingke/php72Dockerfile "链接地址")

### php5.6.38 dockerfile 编译文件
[链接地址](https://gitee.com/myxingke/php5.6.38-dockerfile "链接地址")

### 关于windows

### 第一步，获取项目代码
```
git clone https://gitee.com/myxingke/danmp.git
```

### 第二步 修改php运行挂载目录
由于是我自己在使用我默认挂载的是我本机地址，你需要修改一下或者换成你本地的www目录

### 第三步，启动运行
```
cd danmp
//创建容器
docker-compose up -d
```

### 第四步，查看结果

测试nginx：
- http://localhost
- https://localhost

测试apache：
- http://localhost :8080
- https://localhost:4343

如果访问出现以下内容，说明环境搭建成功成功后将会出现如下内容
![Demo Image](./images/demo1.png)

### 关于PHP CURL
配置php72 or php72 or php56 里的extra_host 关于IP 在.evn的配置里
指定ip为当前运行的apache nginx IP
```
extra_hosts:
  - "www.wu.cn:172.100.0.2"
  - "www.spread.cn:172.100.0.2"
  - "shop.du.cn:172.100.0.2"
  - "www.lease.cn:172.100.0.2"
```
每次修改后执行
```
docker-compose restart
单个
docker restart nginx_web
```
重启过后 www.site.com ---> curl www.site1.cn

### docker nginx 反向代理
#### 需要注意两点 在mac windows下 不能直接以子网掩码配置 linux 服务器可以的
代理地址必须是：```host.docker.internal``` \
另还要在server里配置解析器: ```resolver 127.0.0.11 ipv6=off;``` 且ip不能更换 \
[相关文档](https://docs.docker.com/docker-for-mac/networking/#/known-limitations-use-cases-and-workarounds "相关文档")
[相关文档](https://stackoverflow.com/questions/35744650/docker-network-nginx-resolver "相关文档")
具体配置请查看 ```conf/nginx/conf.d/proxy.pc.cn.conf``` 如果反向代理一直不生效可以清除浏览器缓存或刷新dns或换浏览器！之前我就是生效因前面失败导致缓存在一直显示假象。

### PHP 版本切换
vhost配置文件里找到如下内容替换
```
fastcgi_pass   php73:9000; 换成php73版本
fastcgi_pass   php72:9000; 换成php72版本
fastcgi_pass   php56:9000; 换成php5.6版本
```

### MYSQL
程序连接方式
```
- host：(本项目的MySQL容器网络，或mysql)
- port：`3306`
- username：`root`
- password：`root`
```

工具连接方式
```
- host：127.0.0.1
- port：`3306`
- username：`root`
- password：`root`
```

### REDIS
程序连接方式
```
- host: redis
- port: 6379
```
工具连接方式
```
- host: 127.0.0.1
- port: 6379
```

### php 链接示例
```$xslt
//mongo
'Common\Component\Mongo' => [
    'main' => [
        'host'     => '172.100.0.40',
        'port'     => '27017',
        'user'     => 'root',
        'password' => 'root',
        'auth'     => '',
        //库名
        'db_name'  => 'pcbx_nk',
    ]
]
//mysql
'master'    => [
    'db_host'    => '172.100.0.20',
    'db_name'    => 'pcbx_nk',
    'db_user'    => 'root',
    'db_pass'    => 'root',
    'db_port'    => 3306,
    'db_charset' => 'utf8mb4'
]
//redis
'conf' => [
    'host'     => '172.100.0.30',
    'port'     => 6379,
    'db'       => 1,
    'password' => ''
]
```

### portainer
portainer docker的gui管理工具
```
- localhost: 9000
```

### 使用php的composer和php命令

```
//使用php72服务的composer命令
docker-compose exec php72 composer --version
//使用php72服务的php命令行工具
docker-compose exec php72 php -v  
```
### php 开发时可能需要暴露出 端口以便于调试。默认是没有暴露端口的
```
//在 docker ps 下你可以看到 
9000/tcp, 9501/tcp 
而不是
0.0.0.0:9000/tcp, 0.0.0.0:9501->9501/tcp 

需要在docker-compose php container_name 里增加如下暴露端口
ports:
    - "9000:9000"
    - "9501:9501"

添加后你可以直接通过 ip:port方式访问
```

### 关于 xdebug扩展
##### 在开发swoole的时候运行swoole服务协程如果开启了xdebug 会有提示如下
```
Swoole\Server::start(): Using Xdebug in coroutines is extremely dangerous, please notice that it may lead to coredump! in
```
##### swoole 会检查是否开启了xdebug 如果有强迫症的人，关闭掉xdebug 修改 
```
conf/php/ext/php73/docker-php-ext-xdebug.ini
直接注释掉前面加个分号
;zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20180731/xdebug.so
如果要开启取消掉前面的;分号
重启服务即可
```

### php 运行权限
## 在windows与mac上开发的时候docker自动映射了权限，在linux当中docker与宿主机权限隔离，有可能宿主机是root 但在容器里运行的php-fpm是www-data。
## 这个时候需要你在宿主机上也创建一个www-data用户组与用户
```$xslt
useradd www-data
useradd -g www-data www-data
```
### 如果你的 要创建文件夹以及生成缓存等文件。需要你在你的linux宿主机把相应的目录给上对应的用户及组.如果不生成文件这些到无所谓了
```
chown -R www-data:www-data public/ logs/
```

### php 已安装扩展
##### php5.6/7.2基本只有一两个扩展差距，如果觉得有差的你也可以提issues或者在上面提供的地址自己编译
bcmath \
calendar \
Core \
ctype \
curl \
date \
dom \
exif \
fileinfo \
filter \
ftp \
gd \
gettext \
hash \
iconv \
json \
libxml \
mbstring \
memcached \
mongodb \
mysqlnd \
mysqli (5.6) \
openssl \
pcntl \
pcre \
PDO \
pdo_mysql \
pdo_pgsql \
pdo_sqlite \
Phar \
posix \
readline \
redis \
Reflection \
session \
shmop \
SimpleXML \
soap \
sockets \
sodium \
SPL \
sqlite3 \
standard \
swoole \
sysvmsg \
sysvsem \
sysvshm \
tokenizer \
wddx \
xdebug \
xml \
xmlreader \
xmlrpc \
xmlwriter \
xsl \
Zend OPcache \
zip \
zlib \
[Zend Modules] \
Xdebug \
Zend OPcache

## 容器相关命令管理

1. 查看运行的容器

```
//如果state的状态为up 说明容器启动成功
docker-compose ps
```
2. 启动、重启、停止 服务

```
//启动所有服务
docker-compose  start
//停止所有服务
docker-compose  stop
//重启所有服务
docker-compose restart
```
3. 查看服务的日志

```
//查看所有服务的日志
docker-compose logs
//查看nginx的服务日志
docker-compose logs nginx
//查看apache
docker-compose logs apache
//查看mysql
docker-compose logs mysql
//查看php72
docker-compose logs 72
```

4. 进入到某个服务

```
//进入apache服务的命令
docker-compose exec apache bash
```

### 容器镜像删除停止命令
```
// 查看所有正在运行容器
docker ps
// containerId 是容器的ID
docker stop containerId
// 查看所有容器
docker ps -a
// 查看所有容器ID
docker ps -a -q 
// stop停止所有容器
docker stop $(docker ps -a -q)
// remove删除所有容器
docker rm $(docker ps -a -q) 
//杀死所有正在运行的容器
docker kill $(docker ps -a -q)
//删除所有未打 dangling 标签的镜像
docker rmi $(docker images -q -f dangling=true)
删除所有镜像
docker rmi `docker images -q`
```

### 移除composer镜像
在php里我已经装好了composer 关于使用方法上面有说明，如果你要单独使用composer把下面命令复制放到docker-compose里吧
```
composer:
container_name: 'composer'
image: composer/composer:php7
command: install
volumes:
  - ${WORKSPACES}:/var/www/:rw
```

### portainer
asset 目录下放的是portainer汉化资源文件\
如果忘记portainer密码 删除 ```data/protainer``` 目录文件资源 重新访问再次输入密码即可解决
